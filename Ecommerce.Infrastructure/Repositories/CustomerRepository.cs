﻿using Ecommerce.Core.Context;
using Ecommerce.Core.Entities;
using Ecommerce.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Infrastructure.Repositories
{
    public class CustomerRepository:ICustomerRepository
    {
        private readonly AppDbContext _appDbContext;

        public CustomerRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<IEnumerable<Customer>> GetAll()
        {
            try
            {
                return await _appDbContext.Customers.AsNoTracking().ToListAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<Customer> Insert(Customer customer)
        {
            try
            {
                var insertedCustomer = await _appDbContext.AddAsync(customer);
                await _appDbContext.SaveChangesAsync();
                return insertedCustomer.Entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<Customer> GetById(int id)
        {
            Customer customer = await _appDbContext.Customers.Select(x => new Customer()
            {
                Id = x.Id,
                FirstName = x.FirstName
            }).SingleAsync(x => x.Id == id);
            return customer;
        }
    }
}
