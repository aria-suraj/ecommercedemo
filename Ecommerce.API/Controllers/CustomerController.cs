﻿using Ecommerce.Business.DTOS;
using Ecommerce.Business.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet, Route("~/api/customers")]
        public async Task<IActionResult> GetAllCustomer()
        {
            try
            {
                return Ok(await _customerService.GetAllCustomer());
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
