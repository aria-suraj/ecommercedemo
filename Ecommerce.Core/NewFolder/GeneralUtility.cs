﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Core.NewFolder
{
    public static class GeneralUtility
    {
        public static string GetDatabaseName(IConfiguration configuration)
        {

            System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder
            {
                ConnectionString = configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value
            };
            ;
            return builder["Database"] as string;
        }
    }
}
