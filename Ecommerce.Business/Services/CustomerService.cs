﻿using AutoMapper.Internal.Mappers;
using Ecommerce.Business.DTOS;
using Ecommerce.Business.Mapper;
using Ecommerce.Core.Entities;
using Ecommerce.Core.NewFolder;
using Ecommerce.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Business.Services
{
    public interface ICustomerService
    {
        Task<IEnumerable<CustomerSelectDto>> GetAllCustomer();
        Task<CustomerSelectDto> InsertCustomer(CustomerInsertDto customerInsertDto);
    }
    public class CustomerService:ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task<IEnumerable<CustomerSelectDto>> GetAllCustomer()
        {
            try
            {
                var customers = await _customerRepository.GetAll();
                return ObjectMapper.Mapper.Map<IEnumerable<CustomerSelectDto>>(customers);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<CustomerSelectDto> InsertCustomer(CustomerInsertDto customerInsertDto)
        {

            try
            {
                Customer customer = ObjectMapper.Mapper.Map<Customer>(customerInsertDto);
                return ObjectMapper.Mapper.Map<CustomerSelectDto>(await _customerRepository.Insert(customer));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}
