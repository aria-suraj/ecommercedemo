﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Business.DTOS
{
    public class CustomerDto
    {
        public string FirstName { get; set; } = null!;

    }

    public class CustomerInsertDto : CustomerDto
    {
        public int Id { get; set; }
    }

    public class CustomerSelectDto : CustomerDto
    {
        public int Id { get; set; }
    }

}
