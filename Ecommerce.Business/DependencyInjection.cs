﻿using Ecommerce.Business.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Business
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddServicesFromBusiness(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddTransient<ICustomerService, CustomerService>();
            return services;
        }
    }
}
